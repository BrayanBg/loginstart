const signUpButtom = document.getElementById("signUp");
const signInButtom = document.getElementById("signIn");
const container = document.getElementById("container");

signUpButtom.addEventListener("click", () => {
  container.classList.add("right-panel-active");
});

signInButtom.addEventListener("click", () => {
  container.classList.remove("right-panel-active");
});

document.getElementById('Fsignin').addEventListener('submit', async ()=>{
  let mail = document.getElementById('IMail').value;
  let pass = document.getElementById('IPass').value;
  fetch('http://localhost:3000/cuenta/login', {
      method: 'POST',
      body: JSON.stringify({
        email: mail,
        pass: pass
      }),
      headers: {
        "Content-type": "application/json"
      }
  })
    .then(response => response.json())
    .then(json => console.log(json))
});

document.getElementById('Fsignup').addEventListener('submit', async ()=>{
  let mail = document.getElementById('rMail').value;
  let pass = document.getElementById('rPass').value;
  let name = document.getElementById('rName').value;
  let lastname = document.getElementById('rLName').value;
  await fetch('http://localhost:3000/cuenta/register', {
      method: 'POST',
      body: JSON.stringify({
        email: mail,
        pass: pass,
        name: name,
        lastname: lastname
      }),
      mode: 'cors',
      headers: {
        "Content-type": "application/json",
        'Access-Control-Allow-Origin': '*'
      }
  })
    .then(response => response.json())
    .then(json => console.log(json))
});