const express = require('express');
const cuentaRoute = require('./src/routes/cuenta.route');
const app = express();
const port=3000;

require('./src/DB/db');

app.use(express.json());

app.use('/cuenta', cuentaRoute);

app.get('/hello', (req, res) => {
    res.status(200).json("HELLO WORLD");
});

app.listen(port, () => { console.log(`Escuchando en el puerto ${port}`) });

module.exports = app;
