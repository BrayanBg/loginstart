const User = require('../DB/models/usuario.model');

const Users = async (req, res) => {
    try {
        const user = await User.find();
        res.json(user);
    } 
    catch (err) { 
        res.status(404).json(err); 
    }  
}

const loginUser = async (req, res) => {
    try{
        const {email , pass} = req.body;
        const {email: users, password: passw} = await User.findOne({email: email});
        validate = users == email ? true:false;
        if(validate) validate = pass == passw ? true:false;
        if(validate) {
            res.json('OK');
        }
        else{
            res.status(404).json('Credenciales invalidas');
        }
     }
     catch(err){
        res.status(500).json(err);
     }
}

const add = async (req, res) => {
    try{
        const {email , pass, lastname, name} = req.body;
        const validate = await User.findOne({email: email});
        if(validate){
            res.status(409).json('El usuario ya existe');
        }else{
            let user;
            user.email = email;
            user.password = pass;
            user.name = name;
            user.lastname = lastname;
            const aux = new User(user);
            await aux.save();
            res.status(201).json(await User.findOne({username: user.username}));
        }
    }
    catch(err){
        console.log(err);
        res.status(400).json(err);
    }    
}

module.exports = { Users, add, loginUser }