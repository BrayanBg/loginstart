const express = require('express');
const User = require('../controller/usuario.controller');
const router = express.Router();


router.post('/register', User.add);
router.post('/login', User.loginUser);

module.exports = router
